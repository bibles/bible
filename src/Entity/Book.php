<?php

namespace App\Entity;

use App\Entity\Traits\AbbreviationTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\NameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    use IdTrait;
    use NameTrait;
    use AbbreviationTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bible", inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Verse", mappedBy="book")
     */
    private $verses;

    public function __construct()
    {
        $this->verses = new ArrayCollection();
    }

    public function getBible(): ?Bible
    {
        return $this->bible;
    }

    public function setBible(?Bible $bible): self
    {
        $this->bible = $bible;

        return $this;
    }

    /**
     * @return Collection|Verse[]
     */
    public function getVerses(): Collection
    {
        return $this->verses;
    }

    public function addVerse(Verse $verse): self
    {
        if (!$this->verses->contains($verse)) {
            $this->verses[] = $verse;
            $verse->setBook($this);
        }

        return $this;
    }

    public function removeVerse(Verse $verse): self
    {
        if ($this->verses->contains($verse)) {
            $this->verses->removeElement($verse);
            // set the owning side to null (unless already changed)
            if ($verse->getBook() === $this) {
                $verse->setBook(null);
            }
        }

        return $this;
    }
}
