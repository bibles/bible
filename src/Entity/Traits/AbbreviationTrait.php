<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait AbbreviationTrait
{
    /**
     * @ORM\Column(type="string", length=15, unique=true)
     *
     */
    private $abbreviation;

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }
}
