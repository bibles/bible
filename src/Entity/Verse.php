<?php

namespace App\Entity;

use App\Entity\Traits\AbbreviationTrait;
use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VerseRepository")
 */
class Verse implements \JsonSerializable
{
    use IdTrait;
    use AbbreviationTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book", inversedBy="verses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $book;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"="true"})
     */
    private $chapter;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"="true"})
     */
    private $number;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getChapter(): ?int
    {
        return $this->chapter;
    }

    public function setChapter(int $chapter): self
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function jsonSerialize()
    {
        return $this->text;
    }
}
