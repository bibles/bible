<?php

namespace App\Entity;

use App\Entity\Traits\AbbreviationTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\NameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BibleRepository")
 */
class Bible
{
    use IdTrait;
    use NameTrait;
    use AbbreviationTrait;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Book", mappedBy="bible")
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setBible($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->contains($book)) {
            $this->books->removeElement($book);
            // set the owning side to null (unless already changed)
            if ($book->getBible() === $this) {
                $book->setBible(null);
            }
        }

        return $this;
    }
}
