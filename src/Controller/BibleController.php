<?php

namespace App\Controller;

use App\Repository\VerseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BibleController extends AbstractController
{
    /**
     * @Route("/{reference}", name="reference", methods={"GET"})
     */
    public function reference($reference, VerseRepository $repository)
    {
        // TODO parse the reference with different strategies and use the correct repository method
        // TODO order by verse number
        // $strategy->parse($reference);

        $verses = $repository->findBy([
            'chapter' => 1
        ]);

        return $this->json($verses);
    }
}
