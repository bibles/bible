# Symfony Microservice Skeleton

- Extra bundles: framework-extra, monolog, orm, security, serializer & validator, doctrine-fixtures-bundle, test-pack & web-server
- PHPUnit configuration for unit, integration and functional tests
- Doctrine Fixtures bundle
- /build/ folder ignored
- Setup .env.test
- Setup .gitlab-ci.yml with MySQL service
- Docker image for project registry
