<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BibleControllerFunctional extends WebTestCase
{
    /** @var KernelBrowser */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function test_whole_chapter()
    {
        $client = static::createClient();

        $client->request('GET', '/GEN.1.KJV');

        $response = \json_decode($client->getResponse()->getContent());

        $this->assertResponseIsSuccessful();

        static::assertSame(
            'In the beginning God created the heaven and the earth.',
            $response[0]
        );

        static::assertCount(31, $response);
    }
}
